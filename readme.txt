*Procedurally Generated Fractal Terrain*

Uses the Diamond Square Algorithm with n=9 iterations to generate a height map which is then applied to a terrain object
in Unity by calling setHeights with the height map as a parameter.

The resulting terrain is then given appropriate textures using Unity's alphamaps while accounting for height segregation.
Segregation of heights is based on the number of alphamapLayers (number of Textures added to terrain).

This particular implementation has strict bounds with regards to variance in the Diamond Square Algorithm random offsets
and as such rarely produces terrain with heights that are far off from each other.
(ie rarely see snow cap mountain together with sand)

The camera is attached to a rigidbody sphere to handle collisions with terrain, however if the rigidbody is moving too
quickly, the colliders will allow it to fallthrough terrain. As a result, the flight speed has been set to be quite
slow and traversing the entire terrain may become frustrating.

Water uses the Legacy Shader Transparent Diffuse with Alpha values set appropriately.
There is also another Water implementation taken from the Unity Standard Assets 'Water4 but is not set to appear in
the main scene at this time.

The shader has not yet been implemented at this time.

Some code has been implemented from these sources:

Diamond Square Algorithm from -http://www.gameprogrammer.com/fractal.html#diamond
Modulo part for wrapping around the list-http://stackoverflow.com/questions/2755750/diamond-square-algorithm
Alphamapping for Texturing Terrain-https://alastaira.wordpress.com/2013/11/14/procedural-terrain-splatmapping/
Texture Shader -https://en.wikibooks.org/wiki/Cg_Programming/Unity/Lighting_Textured_Surfaces

Every attempt has been made to understand the code and algorithms implemented in these sources.