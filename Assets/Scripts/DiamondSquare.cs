﻿using UnityEngine;
using System.Collections;

// Algorithm from -http://www.gameprogrammer.com/fractal.html#diamond
// Modulo part for wrapping -http://stackoverflow.com/questions/2755750/diamond-square-algorithm
// Alphamapping -https://alastaira.wordpress.com/2013/11/14/procedural-terrain-splatmapping/

public class DiamondSquare : MonoBehaviour {

    public Terrain terrain;

    // The diamond-square algorithm uses a grid of size 2^n + 1

    public float n = 9f;
    float gridSize;
    int side;
    int step;

    // determines how high and how low the height values can get
    public float variance = 0.5f;
    float noise;

    float[,] heightMap;

    // Segment the Height so that we can produce:
    // - Rocky outcrops or snow on top of mountains
    // - Grass or soil in valleys

    // Semi-transparent water sections made as a plane through the landscape

    // Use this for initialization
    void Start() {

        gridSize = Mathf.Pow(2f, n) + 1;
        side = (int)gridSize - 1;

        DiamondSquareAlgorithm();
        GenerateTerrain();
        ColourTerrain();
    }

    void DiamondSquareAlgorithm()
    {
        heightMap = new float[side + 1, side + 1];

        // initialise the 4 corners of the terrain
        heightMap[0, 0] = 0.5f;
        heightMap[side, 0] = 0.5f;
        heightMap[0, side] = 0.5f;
        heightMap[side, side] = 0.5f;

        int depth = side;
        float height;

        while (depth >= 2)
        {
            // Location of the point we are working on for Diamond Step
            // and also the location of the points that will be averaged for the Square Step
            step = depth / 2;

            // Diamond Step
            // X     X 
            //
            //    O
            //
            // X     X
            for (int i = step; i < side; i += depth)
            {
                for (int j = step; j < side; j += depth)
                {
                    // Random.value returns a random value from 0.0 to 1.0 inclusive - from unity documentation
                    noise = (Random.value * 2 * variance) - variance;

                    height = heightMap[i - step, j - step];
                    height += heightMap[i + step, j - step];
                    height += heightMap[i - step, j + step];
                    height += heightMap[i + step, j + step];

                    height /= 4.0f;

                    height = Mathf.Clamp01(height + noise);
                    //height = noise > 0 ? Mathf.Clamp01(height + noise) : height + noise;
                    //height += noise;

                    heightMap[i, j] = height;
                }
            }
            // Square Step
            //     X
            //  X  O  X
            //     X

            for (int i = 0; i < side; i += step)
            {
                for (int j = (i + step) % depth; j < side; j += depth)
                {
                    noise = (Random.value * 2 * variance) - variance;

                    // Uses Modulo to wrap around to the other side of the terrain
                    height = heightMap[(i - step + side) % (side), j];
                    height += heightMap[(i + step) % (side), j];
                    height += heightMap[i, (j + step) % (side)];
                    height += heightMap[i, (j - step + side) % (side)];

                    height /= 4.0f;

                    height = Mathf.Clamp01(height + noise);
                    //height = noise > 0 ? Mathf.Clamp01(height + noise) : height + noise;
                    //height += noise;

                    heightMap[i, j] = height;

                    if (i == 0) heightMap[side, j] = height;
                    if (j == 0) heightMap[i, side] = height;
                }
            }
            // Go deeper into the height map iteration
            depth = depth / 2;
            // Reduce variance since we are working on increasingly closer points
            variance = variance / 2;
        }

    }

    void GenerateTerrain()
    {
        var terraData = terrain.terrainData;

        terraData.SetHeights(0, 0, heightMap);
    }
    
    void ColourTerrain()
    {
        TerrainData terrainData = terrain.terrainData;

        float[,,] splatmapData = new float[terrainData.alphamapWidth, terrainData.alphamapHeight, terrainData.alphamapLayers];

        // Normalize x and y coordinates to the height map size for indexing
        float y_01 = Normalizer(side, terrainData.alphamapHeight);
        float x_01 = Normalizer(side, terrainData.alphamapWidth);

        float textureBlend;

        int textureID;

        float textureWeight;

        // linearisation of stepped height values based on number of texture layers available
        float heightSteps = 1f / (terrainData.alphamapLayers - 1);

        for (int y = 0; y < terrainData.alphamapHeight; y++)
        {
            for (int x = 0; x < terrainData.alphamapWidth; x++)
            {

                // Setup an array to record the mix of texture weights at this point
                float[] splatWeights = new float[terrainData.alphamapLayers];
                

                // The intermediate value between textures at any given height
                textureBlend = (heightMap[Mathf.RoundToInt(x * x_01), Mathf.RoundToInt(y * y_01)]) / heightSteps;

                textureID = (int)textureBlend;

                // obtain the decimal part of the blend
                textureWeight = textureBlend - textureID;

                // if the texture weight heavily lies on the lower ID
                if (textureWeight < 0.2f)
                {
                    // we set the splat weight of the lower texture ID to be high
                    textureWeight = 0f;
                }
                // otherwise the texture weight is loosely related to the next texture ID and can be smoothed
                else
                {
                    // the value of the 'smoothing' of the lower ID texture when mixed with the higher ID texture
                    textureWeight = Mathf.InverseLerp(0.2f, 1f, textureWeight);
                }

                // set the lower ID texture weight
                splatWeights[textureID] = 1f - textureWeight;

                // if there is a higher ID texture available, we set the other half of the smoothing value to that texture ID
                if (textureID < terrainData.alphamapLayers - 1)
                {
                    splatWeights[textureID + 1] = textureWeight;
                }

                // Loop through each terrain texture
                for (int i = 0; i < terrainData.alphamapLayers; i++)
                {
                    // Assign this point to the corresponding splatmapData layer
                    splatmapData[x, y, i] = splatWeights[i];
                }
            }
        }

        // Finally assign the new splatmap to the terrainData:
        terrainData.SetAlphamaps(0, 0, splatmapData);
    }
    

    float ListSum (float [] listed)
    {
        float sum = 0;
        for (int i = 0; i < listed.Length; i++)
        {
            sum += listed[i];
        }
        return sum;
    }

    float Normalizer (int a, int b)
    {
        return (float)a / (float)b;
    }

}
