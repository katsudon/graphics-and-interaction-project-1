﻿using UnityEngine;
using System.Collections;

public class FlightControls : MonoBehaviour
{

    public float flightSpeed = 50.0f; //default speed sentitivity
    public float mouseSpeed = 50.0f;


	// Use this for initialization
	void Start () {
	
	}

    // Update is called once per frame
    void Update()
    {
        //Translate uses Space.Self by default

        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(Vector3.left * flightSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector3.right * flightSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(Vector3.forward * flightSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(Vector3.back * flightSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.Q))
        {
            transform.Rotate(Vector3.forward * (flightSpeed/2) * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.E))
        {
            transform.Rotate(Vector3.back * (flightSpeed/2) * Time.deltaTime);
        }
        transform.Rotate(Vector3.up * Input.GetAxis("Mouse X") * mouseSpeed * Time.deltaTime);
        transform.Rotate(Vector3.left * Input.GetAxis("Mouse Y") * mouseSpeed * Time.deltaTime);

        boundCamera();

    }

    void boundCamera()
    {
        while (transform.position.x < 0)
        {
            transform.position += Vector3.right;
        }
        while (transform.position.x > 2049)
        {
            transform.position -= Vector3.right;
        }
        while (transform.position.z < 0)
        {
            transform.position += Vector3.forward;
        }
        while (transform.position.z > 2049)
        {
            transform.position -= Vector3.forward;
        }
        while (transform.position.y < 0)
        {
            transform.position += Vector3.up;
        }
        while (transform.position.y > 1000)
        {
            transform.position -= Vector3.up;
        }
    }
}
