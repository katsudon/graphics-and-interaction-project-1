﻿using UnityEngine;
using System.Collections;

public class SunMovement : MonoBehaviour {

    public float moveSpeed;
    Vector3 midpoint;

	// Use this for initialization
	void Start () {
        midpoint = new Vector3(1700f, 0f, 1025f);
	}
	
	// Update is called once per frame
	void Update () {
        transform.RotateAround(midpoint, Vector3.right, moveSpeed * Time.deltaTime);
    }
}
